---
header-includes:
   - \usepackage{graphicx}
---
\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth]{./pics/profile_pic_1.jpg}
\end{figure}

<img src="pics/profile_pic_1.jpg" height="200" width="200" align="right" />

# Peter Schura
*Senior Software Engineer*

**Tel.**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+49 152 5446 2125  
**E-Mail**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;peters.mbox@gmail.com  
**Anschrift** &nbsp;Emmentaler Str. 101b, 13409 Berlin

# Über mich

Ich bin ein erfahrener und engagierter Software-Ingenieur mit umfangreicher Erfahrung in der Backend- und Middleware-Entwicklung. Im Laufe meiner Karriere habe ich vielseitige Fähigkeiten in Java, Go, JavaScript und anderen Programmiersprachen und Frameworks wie SpringBoot und Node.js entwickelt und in verschiedenen Branchen wie Banken, Gaming und Einzelhandel gearbeitet.

Ich habe einen Abschluss als Diplom-Informatiker (FH) von der Technischen Hochschule Ingolstadt und bin in mehreren professionellen Standards zertifiziert, darunter SCRUM, Softwarearchitektur und mehrere IBM-Entwicklungsframeworks.

In meinen letzten Projekten habe ich durch die Entwicklung digitaler Plattformen, die Automatisierung von Geschäftsprozessen und die Leitung von Software-Entwicklungsteams einen wichtigen Beitrag zur Bereitstellung hochwertiger, innovativer Lösungen geleistet. Meine Herangehensweise an die Softwareentwicklung zeichnet sich durch eine Verpflichtung zu sauberem Code, agilen Methoden und kontinuierlichem Lernen und Verbessern aus.

Mein berufliches Ethos basiert auf aufschlussreichen Problemlösungen, bei denen ich mich darauf konzentriere, die Grundursachen zu identifizieren und nachhaltige und innovative Lösungen anzubieten. Ich engagiere mich für die Zusammenarbeit im Team und die kundenorientierte Lieferung.


# Berufliche Laufbahn

11/2021 - 04/2024  
**[Emma – The Sleep Company](https://www.emma-sleep.com/), Remote**  
*Golang Developer (Contract)*
- Entwicklung von Microservices (Middleware) in Go
- Automatisierung von Arbeitsabläufen
- Testautomatisierung (E2E Tests mit Cucumber, Unit Tests)
- Entwicklung von Tools zur Produktivitätssteigerung und Behebung von Produktionsfehlern mit JavaScript und Node.js
- Code-Reviews und Onboarding von neuen Teammitgliedern

05/2020 - 11/2021  
**[Gamomat Development GmbH](https://gamomat.com/), Berlin**  
*Senior Software Engineer*
- Entwicklung von Slot-Spielen
- Integration der Spiele in die Casinos
- Entwicklung von Tools und Automatisierung von internen Prozessen
- Code-Reviews
  
03/2016 - 03/2020  
**[Westernacher Solutions GmbH](https://westernacher-solutions.com/), Berlin**  
*Berater / Teamleiter*
- Betreuung des Kunden als Berater (SOA)
- Implementierung von Notariatssoftware
- Führen eines Entwicklerteams
- Code-Reviews

02/2013 - 02/2016  
**[Bank Soft Systems](https://bssys.com/eng/), Moskau**  
*Software Architekt / Teamleiter*
- Entwurf und Implementierung von Dienstleistungen mit Schwerpunkt auf Online-Zahlungsmethoden
- Leitung eines Teams von Entwicklern
- Code-Reviews

10/2011 - 01/2013  
**[Bank Soft Systems](https://bssys.com/eng/), Moskau**  
*Senior Java Developer*
- Anwendungsintegration für staatliche Behörden
- Entwicklung von Webservices

08/2008 - 09/2011  
**[Cinimex](https://cinimex.ru/en/), Moskau**  
*Senior Software Developer* 
- Anwendungsintegration für Banken
- Entwicklung von Hochlastdiensten auf Basis von WebSphere MQ, WebSphere Message Broker

02/2007 - 07/2008  
**Purventis GmbH, Ingolstadt**  
*Junior Software Developer*  
- Entwicklung von Variantenmanagement Software in .NET and C#

# Studium

2005 - 2011  
**[Technical University Ingolstadt (THI)](https://www.thi.de/)**  
Abschluss zum Diplom-Informatiker (FH)  
Prüfungsgesamtnote: 1,8

# Zertifikate

- Programming with Google Go (UC Irvine)
- ITEMO Professional SCRUM Master (TÜV SÜD Akademie)
- Certified Professional for Software Architecture - Foundation Level (ISAQB)
- IBM Certified Solution Developer - WebSphere Message Broker (IBM)
- IBM Certified Solution Developer - WebSphere Integration Developer (IBM)

# Sprachen

Deutsch (fließend), Englisch (fließend), Russisch (Muttersprache)

# Fachkompetenzen

`Software Architektur` `Architekturdokumentation (Arc42)` `Agile` `Teamleitung` `Clean Code` `Code Reviews` `Backend-Entwicklung` `Frontend-Entwicklung` `Testautomatisierung` `Continuous Integration / Continuous Delivery (CI/CD)` `GitLab` `Maven` `Gradle` `Enterprise Application Integration` `Microservices` `Service Oriented Architecture` `REST` `SOAP` `Java` `Groovy` `JavaScript` `TypeScript` `Node.js` `Go` `Docker` `Datenbanken`

# Projekte

11/2021 - 04/2024  
**Emma – The Sleep Company, Remote**  
In meiner Rolle als Golang-Entwickler bei dem dynamisch wachsenden internationalen Unternehmen Emma Sleep helfe ich bei der Entwicklung einer neuen digitalen Plattform zur Automatisierung der Geschäftsprozesse des Unternehmens. Ich modelliere den Prozess der Auftragsabwicklung in Fluent Commerce (OMS) und entwickle Middleware-Microservices in der Programmiersprache Go. Ich schreibe Skripte in JavaScript und Node.js, um die Leistung des Teams bei der Implementierung neuer Funktionen und der Lösung von Produktionsproblemen zu steigern. Zu meinen Erfolgen im Unternehmen gehört auch das Entwerfen und Implementieren von E2E-Tests auf Basis von Cucumber und Java mit SrpingBoot. Ich identifiziere Engpässe in der aktuellen Architektur und schlage strategische Verbesserungen vor.

Technologien: `Go`, `Docker`, `AWS`, `Java`, `Cucumber`, `SpringBoot`, `FluentCommerce`, `Postman` `JavaScript` `Node.js`

05/2020 - 11/2021  
**Gamomat Development GmbH**  

Als Senior Software Engineer in einem dynamisch wachsenden Unternehmen helfe ich, die bestehenden Spiele und die dazugehörige Infrastruktur weiterzuentwickeln und in die Cloud zu bringen. Dabei habe ich Einblick in fast alle technischen Aspekte wie Frontend- und Backend-Entwicklung, CI/CD, Entwicklung von Tools und Modernisierung bestehender Anwendungen, Anbindung an externe Provider, Cloud, etc. Auch die Prozessoptimierung gehört zu meinen Aufgaben.

Technologien: `Java` `Go` `JavaScript` `TypeScript` `CI/CD` `.NET` `Apache Maven` `GitLab CI` `Jenkins` `Docker` `Kubernetes`

01/2018 – 12/2019  
**XNotar 4, Westernacher Solutions GmbH**

In der Rolle eines technischen Projektleiters (Leitung eines Scrum-Teams, bestehend aus 5-6 Entwicklern) war ich an der Entwicklung einer spezialisierten Software für Notare auf Basis von Electron (mit Chromuium + Node.js) beteiligt.

Technologien: `Microservices` `Spring Boot` `Angular` `Continuous Integration` `Test-Automatisierung` `Release-Automatisierung` `TypeScript` `JavaScript` `Java` `Groovy` `Gradle`

03/2016 - 12/2017  
**Enterprise Webservices Platform, Westernacher Solutions GmbH**  

Als Berater für Service Orientiere Architekturen und Anwendungsintegration unterstützte ich unseren Kunden (Bundesnotarkammer) bei der Konzeption und Entwicklung der Webservices Platform, die die Fachanwendungen der Kammer integriert.

Technologien: `Webservices` `Oracle Enterprise Service Bus`

01/2013 – 02/2016  
**EBPP (electronic bill presentment and payment)**  

Als Software Architekt und Teamleiter eines agilen Teams bestehend aus 5-7 Entwicklern war ich für die Projektierung und Umsetzung von Diensten mit Schwerpunkt Online-Zahlungsmethoden verantwortlich. Das entwickelte Produkt ermöglicht es den Bankkunden Dienste verschiedener Anbieter (Versorgungsunternehmen, Mobilfunkanbieter, Online-Shops etc.) online zu bezahlen.

Technologien: `Apache ServiceMix` `Java` `JavaScript (Angular)` `JEE` `Oracle Enterprise Service Bus`

10/2011 - 01/2013  
**Entwicklung von Webservices für SMEV (Elektronisches Interaktionssystem für staatliche Behörden)**

Als Senior Developer war ich verantwortlich für die Entwicklung von Webservices für das Elektronische Interaktionssystem für staatliche Behörden (SMEV) auf Basis von IBM WebSphere ESB.

Technologien: `IBM WebSphere ESB` `Java`

08/2008 - 09/2011  
**Entwicklung von ausfallsicheren Hochlastdiensten**

Als Senior Softwareentwickler war ich für die Entwicklung von Middleware für den Bankensektor verantwortlich, die eine systemübergreifende Kommunikation ermöglicht. Außerdem habe ich hochbelastbare und ausfallsichere Dienste (Hunderte von RPMs) auf Basis von IBM WebSphere MQ und IBM WebSphere Message Broker entwickelt.

Technologien: IBM WebSphere MQ", "IBM WebSphere Message Broker", "Webservices", "SQL