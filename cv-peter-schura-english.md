---
header-includes:
   - \usepackage{graphicx}
---
\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth]{./pics/profile_pic_1.jpg}
\end{figure}

<img src="pics/profile_pic_1.jpg" height="200" width="200" align="right" />

# Peter Schura
*Senior Software Engineer*

**Phone**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+49 152 5446 2125  
**Email**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;peters.mbox@gmail.com  
**Address**&nbsp;&nbsp;&nbsp;&nbsp;Emmentaler Str. 101b - 13409 Berlin - Germany

# About me

I am an experienced and dedicated Software Engineer with significant expertise in backend and middleware development. Throughout my career, I have developed a versatile skill set in Java, Go, JavaScript, among other programming languages and frameworks like SpringBoot and Node.js, working across various sectors including banking, gaming, and retail.

I hold a Diplom-Informatiker (FH) from the Technical University Ingolstadt and am certified in multiple professional standards including SCRUM, software architecture, and several IBM development frameworks.

In my recent projects, I have made significant contributions by developing digital platforms, automating business processes, and leading software development teams to deliver high-quality, innovative solutions. My approach to software development is characterized by a commitment to clean code, agile methodologies, and continuous learning and improvement.

My professional ethos is built on insightful problem-solving, where I focus on identifying root causes and providing sustainable and innovative solutions. I am committed to team collaboration and customer-focused delivery.

# Professional career

11/2021 - 04/2024  
**[Emma – The Sleep Company](https://www.emma-sleep.com/), Remote**  
*Golang Developer (Contract)*
- Development of Microservices (Middleware) in Go
- Workflow automation
- Test automation (E2E tests with Cucumber, Unit Tests)
- Development of tools for better productivity and solving production bugs with JavaScript and Node.js
- Code reviews and onboarding of new team members

05/2020 - 10/2021  
**[Gamomat Development GmbH](https://gamomat.com/), Berlin**  
*Senior Software Engineer*
- Development of slot games
- Integration of games with the casinos
- Development of tools and automation of internal processes
- Code reviews

03/2016 - 03/2020  
**[Westernacher Solutions GmbH](https://westernacher-solutions.com/), Berlin**  
*Consultant / Team Lead*
- Support of the customer as a consultant (SOA)
- Implementation of notary software
- Leading a team of developers
- Code reviews

02/2013 - 02/2016  
**[Bank Soft Systems](https://bssys.com/eng/), Moscow**  
*Software Architect / Team Lead*
- Design and implementation of services with a focus on online payment methods
- Leading a team of developers
- Code reviews

10/2011 - 01/2013  
**[Bank Soft Systems](https://bssys.com/eng/), Moscow**  
*Senior Java Developer*
- Application integration for state authorities
- Webservice development

08/2008 - 09/2011  
**[Cinimex](https://cinimex.ru/en/), Moscow**  
*Senior Software Developer*
- Application integration for banks
- Development of high-load services based on WebSphere MQ, WebSphere Message Broker

02/2007 - 07/2008  
**Purventis GmbH, Ingolstadt**  
*Junior Software Developer*
- Development of software for handling product variations in .NET and C# 

# Education
2005 - 2011  
**[Technical University Ingolstadt (THI)](https://www.thi.de/)**  
Graduation as Diplom-Informatiker (FH)  
overall grade: 1,8 *(from 1.0 to 6.0 where 1.0 is the best grade)*

# Certificates
- Programming with Google Go (UC Irvine)
- ITEMO Professional SCRUM Master (TÜV SÜD Academy)
- Certified Professional for Software Architecture - Foundation Level (ISAQB)
- IBM Certified Solution Developer - WebSphere Message Broker (IBM)
- IBM Certified Solution Developer - WebSphere Integration Developer (IBM)

# Languages
German (fluent), English (fluent), Russian (native speaker)

# Skills
`Software Architecture` `Architecture Documentation (Arc42)` `Agile` `Team Management` `Clean Code` `Code Review` `Backend Development` `Front-end Development` `Test Automation` `Continuous Integration / Continuous Delivery` `GitLab` `Maven` `Gradle` `Enterprise Application Integration` `Microservices` `Service Oriented Architecture` `REST` `SOAP` `Java` `Groovy` `JavaScript` `TypeScript` `Node.js` `Go` `Docker` `Databases`

# Projects
**Translation Bot - Hobby Project**

Technologies: `Node.js`, `AWS Lambda`, `AWS DynamoDB` `AWS API Gateway` `AWS CDK` `Telegram API` `Telegraf` `Deepl API`

A chatbot to facilitate communication in different languages when using messengers like Telegram: https://t.me/TranslationHelpBot

11/2021 - 04/2024
**Emma – The Sleep Company**

In my role as Golang Developer at the dynamically growing international company Emma Sleep, I am helping to develop a new digital platform to automate the company's business processes. I model the order fulfilment process in Fluent Commerce (OMS) and develop middleware microservices in the Go programming language. I write scripts in JavaScript and Node.js to boost team's performance when implementing new features and solving production incidents. My successes at the company also include designing and implementing E2E tests based on Cucumber and Java with SrpingBoot. I'm identifying bottlenecks of the current architecture and suggesting strategic improvements.

Technologies: `Go`, `Docker`, `AWS`, `Java`, `Cucumber`, `SpringBoot`, `FluentCommerce`, `Postman` `JavaScript` `Node.js`

05/2020 - 10/2021
**Gamomat Development GmbH**

As a Senior Software Engineer in a dynamically growing company I help to further develop the existing games and the associated infrastructure and bring them into the cloud. In doing so, I have insight into almost all technical aspects such as frontend and backend development, CI/CD, development of tools and modernization of existing applications, connection to external providers, cloud, etc. Process optimization is also part of my tasks.

Technologies: `Java` `Go` `JavaScript` `TypeScript` `CI/CD` `.NET` `Apache Maven` `GitLab CI` `Jenkins` `Docker` `Kubernetes`

01/2018 - 12/2019  
**XNotar 4, Westernacher Solutions GmbH**.

In the role of a technical project manager (leading a scrum team consisting of 5-6 developers) I participated in the development of a specialized software for notaries based on Electron (embeds Chromuium + Node.js)

Technologies: `Microservices` `Spring Boot` `Angular` `Continuous Integration` `Test Automation` `Release Automation` `TypeScript` `JavaScript` `Java` `Groovy` `Gradle` `Node.js`

03/2016 - 12/2017  
**Enterprise Webservices Platform, Westernacher Solutions GmbH**

As a consultant for service oriented architectures and application integration I supported our customer (Bundesnotarkammer) in the conception and development of the Webservices Platform, which integrates the services provided by the chamber.

Technologies: `Webservices` `Oracle Enterprise Service Bus`

01/2013 - 02/2016  
**EBPP (electronic bill presentment and payment)**

As a software architect and team lead of an agile team consisting of 5-7 developers, I was responsible for designing the architecture and implementing services with a focus on online payment methods. The developed product enables bank customers to pay online for services of different providers (utilities, mobile operators, online stores etc).

Technologies: `Apache ServiceMix` `Java` `JavaScript (Angular)` `JEE` `Oracle Enterprise Service Bus`

10/2011 - 01/2013  
**Development of SMEV (interdepartmental electronic interaction system) webservices**

As a senior developer I was responsible for developing webservices for the interdepartmental electronic interaction system (SMEV) based on IBM WebSphere ESB.

Technologies: `IBM WebSphere ESB` `Java`

08/2008 - 09/2011  
**Development of failure-resistant high-load services**

As a Senior software developer I was responsible for writing middleware for the banking sector enabling cross-system communication. I also developed high-load and failure-resistant services (hundreds of RPMs) based on IBM WebSphere MQ and IBM WebSphere Message Broker.

Technologies: `IBM WebSphere MQ` `IBM WebSphere Message Broker` `Webservices` `SQL`