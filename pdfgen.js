const markdownpdf = require("markdown-pdf");
const fs = require("fs");
const path = require('path');

const pdfgen = (mdFilePath, pdfFilePath) => {
    const cssFilePath = path.join(__dirname, 'custom-style.css'); // Path to your custom CSS file

    fs.createReadStream(mdFilePath)
        .pipe(markdownpdf({ cssPath: cssFilePath })) // Include the path to your custom CSS file here
        .pipe(fs.createWriteStream(pdfFilePath))
        .on('finish', () => console.log(`PDF file has been created: ${pdfFilePath}`));
}

pdfgen('cv-peter-schura-english.md', 'cv-peter-schura-eng.pdf');
pdfgen('cv-peter-schura-german.md', 'cv-peter-schura-ger.pdf');
